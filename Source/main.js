//Ajax navigation handling
$(document).ready(function() {
    $('#gal').click(function(e){
        e.preventDefault();
        $('.wrap').load('gallery.html #content');
        urlChange("gallery.html");
        setActiveState();
});});

$(document).ready(function() {
    $('#info').click(function(e){
        e.preventDefault();
       $('.wrap').load('index.html #content');
       urlChange("index.html");
       setActiveState();
});});

//Url changing function
var urlChange = function(url) {
    var pathname = location.pathname.slice(0,location.pathname.lastIndexOf('/')+1);   
   history.pushState(pathname+url, "Insys Test",pathname+url);
};

var setActiveState = function(n) {
    var pathname = location.pathname.slice(location.pathname.lastIndexOf('/')+1); 
    if (pathname=="gallery.html") {
         $('#gal').addClass('iconav-activated');
         $('#info').removeClass('iconav-activated');
         getFlickr();
    }  else {
         $('#info').addClass('iconav-activated');
         $('#gal').removeClass('iconav-activated');
    }
    };


//FLICKR API CALL Generates image gallery + stores full size image in alt text
var getFlickr = function(){ 
    var query = 'http://api.flickr.com/services/feeds/photos_public.gne?tags=Marilyn+Monroe&?format=json&?nojsoncallback=1';

    $.ajax({
    data:{format:"json"},
    dataType: "jsonp",
    url: query
    });

    jsonFlickrFeed = function (data) {
    var htmlStr = "";
    $.each(data.items.slice(0,9), function(i,item){
    var thumb = (item.media.m).replace("_m.jpg","_c.jpg");
    var original = (item.media.m).replace("_m.jpg","_b.jpg");
    htmlStr += '<div class="image-wrapper col-md-4 col-xs-6">'
    htmlStr += '<img title="' + item.title + '" src="' + thumb;
    htmlStr += '" alt="' + original + '"class="img-responsive"></div>';
    });
    $('.gallery').html(htmlStr);
    };};

//Activate button at startup and load gallery if required
$(document).ready(function() {
    setActiveState();
});

//Get image 
$(document).ready(function() {
    $('body').on('click','img',function(){
        var alt = $(this).attr('alt');
        if(alt && !document.getElementById('lightbox')) {
        $('#content').append('<img src="'+alt+'" id="lightbox" class="img-responsive fixed centered top black-border">');
        } else {
        $("#lightbox").remove();
        }});});
