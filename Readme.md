Readme

Zadanie zawiera dwie strony:
Index.html czyli stronę zawierającą tekst z cytatem
gallery.html czyli stronę zawierającą galerie

I plik .css i .js.

W Projekcie korzystałem z jQuery i Boostrapa3.
Ładowanie bez reloadu strony zostało rozwiązane przy pomocy $().load() z biblioteki jQuery. Dodatkowo przy każdym ładowaniu dochodzi do zmiany adresu przy pomocy history.pushState. Zostały użyte responsywne ikony w postaci Glyphicon z Bootstrapa. Galeria jest pobierana przy pomocy photos_public.gen z API flickra, pobierane jest 9 najnowszych zdjęć otagowanych Marilyn Monroe. Ze względu na to, że w zadaniu zostałem poproszony o pobieranie miniatur, obrazki są przycinane do kwadrata, dzięki temu też ładniej układają się w gridzie. Po powiększeniu otwierają się obrazki w rozmiarze Large czyli o maksymalnym rozmiarze 1024x768px. Żeby zamknąć obrazek wystarczy kliknąć na jego ponownie. 

Responsive został rozwiązany tak, że gdy zmniejszamy stronę to zmniejsza się padding przy cytacie, tak by ten zajmował cała stronę na komórkach, a nie był tak przycięty z boku jak na załączonych do projektu grafikach. Header z ikonkami zostaje zwiększony, rozmiar samych ikon także  (Na desktopie wygląda na całkiem sporę, ale na komórkach jest idealnie, dlatego załączyłem screencapy).

Ze względu na brak dostępu do grafik użytych na przykładowym projekcie, znalazłem własne. Są one również załączone do projektu.
